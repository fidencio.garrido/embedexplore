package datasize

import (
	"fmt"
	"runtime"
)

const kb = 1024
const mb = 1024 * 1024

func AsKB(sizeInBytes uint64) int {
	return int(sizeInBytes / kb)
}

func AsMb(sizeInBytes uint64) int {
	return int(sizeInBytes / mb)
}

func PrintStats() {
	var m runtime.MemStats
	runtime.ReadMemStats(&m)
	fmt.Printf(`
Objects:   %d
Heap use:  %d Mb
Heap idle: %d Mb

`, m.HeapObjects, AsMb(m.HeapInuse), AsMb(m.HeapIdle))
}
