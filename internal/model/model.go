package model

import (
	"encoding/json"
	"fmt"
)

type Channel struct {
	ID          string
	Name        string
	Number      int
	Description string
}

func New(id int) Channel {
	return Channel{
		ID:          fmt.Sprintf("%d", id),
		Name:        fmt.Sprintf("Channel %d", id),
		Number:      id,
		Description: fmt.Sprintf("The amazing %d channel", id),
	}
}

func FromBytes(input []byte) []Channel {
	var chs []Channel
	json.Unmarshal(input, &chs)
	return chs
}
