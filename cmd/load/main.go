package main

import (
	"ecost/internal/datasize"
	"ecost/internal/model"
	_ "embed"
	"fmt"
	"os"
	"runtime"
)

var (
	channels []model.Channel
)

func loadChannels() {
	data, err := os.ReadFile("./data.json")
	if err != nil {
		panic(err)
	}
	channels = model.FromBytes(data)
}

func main() {
	loadChannels()
	fmt.Printf(`Version Loader

Pre-GC
`)
	datasize.PrintStats()
	runtime.GC()
	fmt.Println("Post-GC")
	datasize.PrintStats()
	fmt.Printf("Channels loaded %d\n", len(channels))
}
