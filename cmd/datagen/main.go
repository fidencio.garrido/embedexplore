package main

import (
	"ecost/internal/model"
	"encoding/json"
	"flag"
	"fmt"
	"os"
)

func main() {
	max := flag.Int("count", 300_000, "Channels to generate")
	flag.Parse()
	chs := make([]model.Channel, *max)
	for i := 0; i < *max; i++ {
		chs[i] = model.New(i)
	}
	data, err := json.Marshal(chs)
	if err != nil {
		panic(err)
	}
	err = os.WriteFile("data.json", data, 0644)
	if err != nil {
		panic(err)
	}
	kb := len(data) / 1024
	fmt.Printf("%d records were written, file size: %d kb\n", *max, kb)
}
