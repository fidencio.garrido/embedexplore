package main

import (
	"ecost/internal/datasize"
	"ecost/internal/model"
	_ "embed"
	"fmt"
	"runtime"
)

var (
	channels []model.Channel

	//go:embed data.json
	source []byte
)

func main() {
	channels = model.FromBytes(source)
	fmt.Printf(`Version Embed
	
Source size:  %d
Pre-GC
`, len(source))
	datasize.PrintStats()
	runtime.GC()
	fmt.Println("Post-GC")
	datasize.PrintStats()
	fmt.Printf("Channels loaded %d\n", len(channels))
}
