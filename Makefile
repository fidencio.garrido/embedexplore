generate:
	go run cmd/datagen/main.go
	cp data.json cmd/emb
	cp data.json cmd/load

run:
	go build -o compiled/emb cmd/emb/main.go
	./compiled/emb
	go build -o compiled/load cmd/load/main.go
	./compiled/load